import Wrapper from "../assets/wrappers/LandingPage";
import main from "../assets/images/main.svg";
import { Link } from "react-router-dom";
import { Logo } from "../components";

const Landing = () => {
  return (
    <Wrapper>
      <nav>
        <Logo />
      </nav>
      <div className="container page">
        <div className="info">
          <h1>
            Job <span>tracking</span> app
          </h1>
          <p>
            A job tracking app is a powerful tool that helps individuals and
            businesses efficiently manage and monitor their job-related
            activities. It provides features such as creating and organizing
            tasks, tracking progress, setting deadlines, assigning
            responsibilities, and generating reports. With a job tracking app,
            users can streamline their workflow, stay organized, and gain
            valuable insights to ensure successful project completion and
            improved productivity.
          </p>
          <Link to="/Register" className="btn register-link">
            Regigter
          </Link>
          <Link to="/Login" className="btn login-link">
            Login / Demo User
          </Link>
        </div>
        <img src={main} alt="Job hunt" className="img main-img" />
      </div>
    </Wrapper>
  );
};

export default Landing;
